console.log("Hello")

// FUNCTIONS in javascript are lines/blocks of codes that tell our device/application to perform certain tasks when called/invoked.

// Functions are mostly create to create complicated tasks to run several lines of code in succession. They are used to prevent repeating lines/blocks of codes that perform the same task/function.


// function declaration
function printName() {
    console.log("My name is John");
};
printName();

// results in an error due to non-existence of the function(the function is not declared)
declaredFunction();

function declaredFunction() {
    console.log("Hello from declaredFuntion");
};

// function expression
let variableFunction = function() {
    console.log("Hello Again");
};
variableFunction();

// Mini activity

function firstFunction() {
    console.log("Top three anime: Jujutsu Kaisen, Demon Slayer, Slamdunk");
};

function secondFunction() {
    console.log("Top three movie: The Transporter, The expendables, The extraction")
}

firstFunction();
secondFunction();

// reassigning declared function

declaredFunction = function() {
    console.log("updated declaredFuntion");
};

declaredFunction();

// changing const funtion will result to failure
const constFunction = function() {
    console.log("initialized const function");
};
constFunction();

// constFunction = function() {
//     console.log("cannot be reassigned");
// };
// constFunction();

// Function scope
{
    let localVar = "Armando Perez";
    console.log(localVar);
}
let globalVar = "Mr. Worldwide";

console.log(globalVar);

function showNames() {
    var functionVar = "Joe";
    const functionConst = "john";
    let functionLet = "Jane";

    console.log(functionVar);
    console.log(functionConst);
    console.log(functionLet);
};
showNames();
// console.log(functionVar);
// console.log(functionConst);
// console.log(functionLet);

// nested functions
function newFunction() {
    let name = "Jane";

    function nestedFunction() {
        let nestedName = "john";
        console.log(nestedName);
    };
    console.log(name);
    nestedFunction();
};

newFunction();
// nestedFunction();

// Mini activity

let globalVariable = "Global";

function myFunction() {
    let myVariable = "local";
    console.log(globalVariable);
    console.log(myVariable);
};

myFunction();

// using alert()

// alert("Hello World");

// function showSample() {
//     alert("Hello Again!");
// };

// showSample();

console.log("I will be displayed after the alert has been closed.");


// using prompt

// let samplePrompt = prompt("Enter your name.")
// console.log("Hello " + samplePrompt);

// let nullPrompt = prompt("do not enter anything here:");
// console.log(nullPrompt);

// Mini Activity

// function welcomeMessage() {
//     let firstPrompt = prompt("Enter your first name:");
//     let lastPrompt = prompt("Enter your last name:");
//     console.log("Hello " + firstPrompt + " " + lastPrompt);
// }

// welcomeMessage();

// Function Namin

function getCourses() {
    let courses = ["Science 101", "Math 101", "English 101"];
    console.log(courses);
};
getCourses();

function get() {
    let name = "Jamie";
    console.log(name);
};

get();

function foo() {
    console.log(25 % 5);
}
foo();