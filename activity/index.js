/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

//first function here:

function personDetails() {
    let fullName = prompt("What is your name: ");
    let age = prompt("How old are you?");
    let address = prompt("Where do you live");
    console.log("Hello, " + fullName);
    console.log("You are " + age + " " + "years old.");
    console.log("You live in " + address);
}
personDetails();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

//second function here:


function myFavBands() {
    console.log("1. Dream Theater");
    console.log("2. Dragonforce");
    console.log("3. 123 Pikit!");
    console.log("4. Queso");
    console.log("5. Greyhoundz");
}
myFavBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

//third function here:

function myFavMovies() {
    console.log("1. The fast and the furious: Tokyo Drift");
    console.log("Rotten Tomatoes Rating: 69%");
    console.log("2. John Wick");
    console.log("Rotten Tomatoes Rating: 86%");
    console.log("The Transporter");
    console.log("Rotten Tomatoes Rating: 73%");
    console.log("The Mechanic");
    console.log("Rotten Tomatoes Rating: 53%");
    console.log("Ender's Game");
    console.log("Rotten Tomatoes Rating: 65%");
}

myFavMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function printUsers() {
    alert("Hi! Please add the names of your friends.");
    let friend1 = prompt("Enter your first friend's name:");
    let friend2 = prompt("Enter your second friend's name:");
    let friend3 = prompt("Enter your third friend's name:");


    console.log("You are friends with:")
    console.log(friend1);
    console.log(friend2);
    console.log(friend3);
};

printFriends();


// console.log(friend1);
// console.log(friend2);